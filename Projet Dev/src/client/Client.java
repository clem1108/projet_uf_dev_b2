package client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client {
	
	public Client() {
		try {
			Socket socket = new Socket("localhost", 1234);
			BufferedReader sock_in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String message = sock_in.readLine();
			System.out.println("Re�u du serveur : " + message);
			socket.close();
			System.out.println("Socket ferm�.");
		} catch(Exception exc) {
			System.err.println(exc.getMessage());
		}
	}

	public static void main(String[] args) {
		new Client();
	}

}
