package serveur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;

import connexion.Connexion;
import exception.ColonnePleineException;
import exception.ToucheExistePasException;
import grille.Jeu;
import thread.threadJeu;

public class ServeurJeu {
	private int nombre_utilisateurs = 0;
	private final int nombreThreadMax = 2;
	private int ordre = 0;
	private static Jeu jeu;
	
	public ServeurJeu() {
		try {
			ServerSocket serveur = new ServerSocket(1234);
			while(true) {
				if (nombre_utilisateurs < nombreThreadMax) {
					System.out.println("Attente d'une connexion...");
					ordre = nombre_utilisateurs;
					Socket socket = serveur.accept();
					nombre_utilisateurs++;
					System.out.println("Nombre utilisateurs connect�s : " + nombre_utilisateurs);
					new Thread(new threadJeu(socket, ordre, this)).start();
				} else {
					Thread.sleep(2000);
				}
			}
		} catch(Exception exc) {
			System.err.println(exc.getMessage());
		}
	}
	
	public int getNombre_utilisateurs() {
		return nombre_utilisateurs;
	}

	public int getNombreThreadMax() {
		return nombreThreadMax;
	}

	public int getOrdre() {
		return ordre;
	}

	public void partir() {
		nombre_utilisateurs--;
		System.out.println("Nombre utilisateurs connect�s : " + nombre_utilisateurs);
	}
	
	public void partie(BufferedReader entrer_socket, PrintWriter sortie_socket, int ordre) {
		jeu = new Jeu();
		boolean selection;
		do {
			sortie_socket.println("Partie classique ou quitter ?");
			selection = true;
			sortie_socket.println("   1   : jouer");
			sortie_socket.println("retour : retour");
			try {
				String ecoute = entrer_socket.readLine();
				switch (ecoute) {
				case "1":
					nom_joueur(entrer_socket, sortie_socket, ordre);
					break;
				case "retour":
					return;
				default:
					selection = false;
					sortie_socket.println("Erreur de frappe");
					break;
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		} while (! selection);
	}
	
	public void jouer(BufferedReader entrer_socket, PrintWriter sortie_socket, int ordre) {
		sortie_socket.println();
		sortie_socket.println("--- DEBUT ---");
		sortie_socket.println();
		boolean tour;
		do {
			do {
				do {
					tour = true;
					sortie_socket.println("Partie numero " + jeu.getNombre_partie_actuel() + "/" + jeu.getNombre_partie() + " - ");
					sortie_socket.println("Tour de " + jeu.get_Nom_Joueur_actuel() + " (" + jeu.get_Symbole_Joueur_actuel() + ")");
					sortie_socket.println();
					if (ordre == jeu.getIndex_joueur_actuel()) {
						jeu.affichage(sortie_socket);
						sortie_socket.println("Entrez le  nom de la case: ");
						try {
							String ecoute = entrer_socket.readLine();
							jeu.jouer(ecoute, ordre);
						} catch (ColonnePleineException e) {
							sortie_socket.println();
							sortie_socket.println("La colonne est pleine. ");
							tour = false;
						} catch (ToucheExistePasException e) {
							sortie_socket.println();
							sortie_socket.println("La touche que vous avez choisit n'est pas bonne.");
							tour = false;
						} catch(IllegalArgumentException e) {
							sortie_socket.println();
							sortie_socket.println("Argument non pris en compte");
							tour = false;
						} catch (Exception e) {
							sortie_socket.println(e);
							sortie_socket.println("Erreur de frappe");
							tour = false;
						} 
					} else {
						try {
							sortie_socket.println("Attente autre joueur.");
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							System.err.println(e.getMessage());
						}
					}
				} while (! tour);
			} while (! jeu.partie_finie(ordre));
			victoire(sortie_socket, ordre);
			jeu.nouvelle_partie();
		} while (jeu.getNombre_partie_restante() > 0);
		
		sortie_socket.println();
		sortie_socket.println("--- Partie terminee ---");
		sortie_socket.println();
		score_final(sortie_socket);
	}
	
	private void victoire(PrintWriter sortie_socket, int ordre) {
		String nom = jeu.getJoueurs(ordre).getNom();
		Connexion connexion = new Connexion("projet_dev_b2");
		String sql = "SELECT * FROM joueur WHERE nom LIKE '" + nom + "'";
		ResultSet resultat = connexion.requete(sql);
		int score = 0;
		try {
			while(resultat.next()) {
				score = resultat.getInt("score");
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		score++;
		String ajout = "UPDATE joueur SET score = '" + score + "'" +
				"WHERE nom LIKE '" + nom + "'";
		connexion.modifier(ajout);
		sortie_socket.println();
		sortie_socket.println("--- Manche terminee ---");
		sortie_socket.println();
		sortie_socket.println("Le joueur " + nom + " a gagne !");
		sortie_socket.println();
		jeu.affichage(sortie_socket);
		sortie_socket.println();
	}
	
	private void score_final(PrintWriter sortie_socket) {
		sortie_socket.println();
		sortie_socket.println("--- Score Final ---");
		sortie_socket.println();
		sortie_socket.println(jeu.info_resultat());
		sortie_socket.println();
	}
	
	public void nom_joueur(BufferedReader entrer_socket, PrintWriter sortie_socket, int index) {
		sortie_socket.println("Entrez le nom du joueur numero "+ index + ": ");
		try {
			String ecoute = entrer_socket.readLine();
			Connexion connexion = new Connexion("projet_dev_b2");
			String sql = "SELECT * FROM joueur WHERE nom LIKE '" + ecoute + "'";
			ResultSet resultat = connexion.requete(sql);
			int nombre = 0;
			try {
				  while (resultat.next()) {
				    nombre++;
				  }
				  if (nombre == 0) {
					  connexion.inserer("INSERT INTO joueur(nom, score) VALUES('" + ecoute + "', 0)");
				  }

			} catch (SQLException e) {
				System.err.println(e.getMessage());
			}
			jeu.getJoueurs(index).setNom(ecoute);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}	
	}

	public static void main(String[] args) {
		new ServeurJeu();
	}

}
