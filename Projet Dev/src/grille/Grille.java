package grille;

import exception.ColonnePleineException;

public class Grille {
	
	private static int nombre_colonnes_defaut = 7;
	private static int nombre_lignes_defaut = 6;

	private Colonne[] grille;
	
	private int nombre_colonnes;
	private int nombre_lignes;
	private int nombre_cases_vides;
    private int colonne_jeton_place = 0;
    private int ligne_jeton_place = 0;
    
    public Grille() {
    	this(nombre_colonnes_defaut, nombre_lignes_defaut);
    }
	
	public Grille(int nombre_colonnes, int nombre_lignes) {
		this.nombre_colonnes = nombre_colonnes;
		this.nombre_lignes = nombre_lignes;
		this.nombre_cases_vides = nombre_colonnes * nombre_lignes;
		this.creation_Grille();
		
	}
	
	public void creation_Grille() {
		this.grille = new Colonne[this.nombre_colonnes];
		for (int index = 0; index < this.nombre_colonnes; index++) {
			this.grille[index] = new Colonne(this.nombre_lignes);
		}
	}
	
	public void nettoyage_Grille() {
		this.creation_Grille();
	    this.nombre_cases_vides = this.nombre_colonnes * this.nombre_lignes;
	}
	
	public void ajouterPion(int indice_colonne, Pion pion) throws IllegalArgumentException, ColonnePleineException {
        this.colonne_jeton_place = indice_colonne;
        this.ligne_jeton_place = this.grille[indice_colonne].getCase_bas_vide();
        this.grille[indice_colonne].ajoute_pion(pion);
        this.nombre_cases_vides--;
    }

	public int getNombre_colonnes() {
		return nombre_colonnes;
	}

	public int getNombre_lignes() {
		return nombre_lignes;
	}

	public char getSymbole(int colonne, int ligne) {
		Case case_selectionner = grille[colonne].getCase(ligne);
		if (! case_selectionner.existe()) {
			return case_selectionner.getPion().getSymbole();
		}
		return Character.MIN_VALUE;
	}

	public int getNombre_cases_vides() {
		return nombre_cases_vides;
	}

	public int getColonne_jeton_place() {
		return colonne_jeton_place;
	}

	public int getLigne_jeton_place() {
		return ligne_jeton_place;
	}
	
	public boolean verification_victoire(Joueur joueur, int nombre_pion_victoire) {
        return (this.verification_diagonales(joueur, nombre_pion_victoire) || this.verification_horizontales(joueur, nombre_pion_victoire)
                || this.verification_verticales(joueur, nombre_pion_victoire));
    }
	
	private boolean verification_horizontales(Joueur joueur, int nombre_pion_victoire) {
       int nombre_aligner = 1;
       nombre_aligner += this.verification_droite(joueur);
       nombre_aligner += this.verification_gauche(joueur);
       if (nombre_aligner >= nombre_pion_victoire) {
           return true;
       } else {
           return false;
       }
    }
	
	private int verification_droite(Joueur joueur) {
        int decalage = 1;
        int nombre_aligner = 0;
        boolean aligner = true;
        if (this.colonne_jeton_place + decalage < this.nombre_lignes) {
            while ((this.colonne_jeton_place + decalage < this.nombre_lignes) && (aligner)) {
                if (! this.grille[this.colonne_jeton_place + decalage].getCase(this.ligne_jeton_place).existe()) {
                    if (this.grille[this.colonne_jeton_place + decalage].getCase(this.ligne_jeton_place).getPion().getJoueur() == joueur) {
                        nombre_aligner++;
                        decalage++;
                    } else {
                        aligner = false;
                    }
                } else {
                    aligner = false;
                }
            }
        }
        return nombre_aligner;
    }
	
    private int verification_gauche(Joueur joueur) {
        int decalage = 1;
        int nombre_aligner = 0;
        boolean aligner = true;
        if (this.colonne_jeton_place - decalage >= 0) {
            while ((this.colonne_jeton_place - decalage >= 0) && (aligner)) {
                if (! this.grille[this.colonne_jeton_place - decalage].getCase(this.ligne_jeton_place).existe()) {
                    if (this.grille[this.colonne_jeton_place - decalage].getCase(this.ligne_jeton_place).getPion().getJoueur() == joueur) {
                    	nombre_aligner++;
                        decalage++;
                    } else {
                    	aligner = false;
                    }
                } else {
                    aligner = false;
                }
            }
        }
        return nombre_aligner;
    }
	
    private boolean verification_verticales(Joueur joueur, int nombre_pion_victoire) {
       int nombre_aligner = 1;
       nombre_aligner += this.verification_bas(joueur);
       if (nombre_aligner >= nombre_pion_victoire) {
           return true;
       } else {
           return false;
       }
    }
    
    private int verification_bas(Joueur joueur) {
        int decalage = 1;
        int nombre_aligner = 0;
        boolean aligner = true;
        if (this.ligne_jeton_place - decalage >= 0) {
            while ((this.ligne_jeton_place - decalage >= 0) && (aligner)) {
                if (! this.grille[this.colonne_jeton_place].getCase(this.ligne_jeton_place - decalage).existe()) {
                    if (this.grille[this.colonne_jeton_place].getCase(this.ligne_jeton_place - decalage).getPion().getJoueur() == joueur) {
                    	nombre_aligner++;
                        decalage++;
                    } else {
                        aligner = false;
                    }
                } else {
                	aligner = false;
                }
            }
            
        }
        return nombre_aligner;
    }
    
    private boolean verification_diagonales(Joueur joueur, int nombre_pion_victoire) {
        return (this.verification_diagonales_vers_bas_gauche(joueur, nombre_pion_victoire) || this.verification_diagonales_vers_bas_droite(joueur, nombre_pion_victoire));
    }
    
    private boolean verification_diagonales_vers_bas_gauche(Joueur joueur, int nombre_pion_victoire) {
       int nombre_aligner = 1;
       nombre_aligner += this.verification_diagonales_haut_droite(joueur);
       nombre_aligner += this.verification_diagonales_bas_gauche(joueur);
       if (nombre_aligner >= nombre_pion_victoire) {
           return true;
       } else {
           return false;
       }
    }

    private boolean verification_diagonales_vers_bas_droite(Joueur joueur, int nombre_pion_victoire) {
       int nombre_aligner = 1;
       nombre_aligner += this.verification_diagonales_haut_gauche(joueur);
       nombre_aligner += this.verification_diagonales_bas_droite(joueur);
       if (nombre_aligner >= nombre_pion_victoire) {
           return true;
       } else {
           return false;
       }
    }
    
    private int verification_diagonales_haut_droite(Joueur joueur) {
        int decalage = 1;
        int nombre_aligner = 0;
        boolean aligner = true;
        if ((this.ligne_jeton_place + decalage < this.nombre_lignes)  && (this.colonne_jeton_place + decalage < this.nombre_colonnes)) {
            while ((this.ligne_jeton_place + decalage < this.nombre_lignes)  && (this.colonne_jeton_place + decalage < this.nombre_colonnes) && (aligner)) {
                if (! this.grille[this.colonne_jeton_place + decalage].getCase(this.ligne_jeton_place + decalage).existe()) {
                    if (this.grille[this.colonne_jeton_place + decalage].getCase(this.ligne_jeton_place + decalage).getPion().getJoueur() == joueur) {
                    	nombre_aligner++;
                        decalage++;
                    } else {
                        aligner = false;
                    }
                } else {
                    aligner = false;
                }
            }
        }
        return nombre_aligner;
    }
    
    private int verification_diagonales_bas_gauche(Joueur joueur) {
    	int decalage = 1;
        int nombre_aligner = 0;
        boolean aligner = true;
        if ((this.ligne_jeton_place - decalage >= 0)  && (this.colonne_jeton_place - decalage >= 0)) {
            while ((this.ligne_jeton_place - decalage >= 0)  && (this.colonne_jeton_place - decalage >= 0) && (aligner)) {
                if (! this.grille[this.colonne_jeton_place - decalage].getCase(this.ligne_jeton_place - decalage).existe()) {
                    if (this.grille[this.colonne_jeton_place - decalage].getCase(this.ligne_jeton_place - decalage).getPion().getJoueur() == joueur) {
                    	nombre_aligner++;
                        decalage++;
                    } else {
                        aligner = false;
                    }
                } else {
                    aligner = false;
                }
            }
        }
        return nombre_aligner;
    }
    
    private int verification_diagonales_haut_gauche(Joueur joueur) {
        int decalage = 1;
        int nombre_aligner = 0;
        boolean aligner = true;
        if ((this.ligne_jeton_place + decalage < this.nombre_lignes)  && (this.colonne_jeton_place - decalage >= 0)) {
            while ((this.ligne_jeton_place + decalage < this.nombre_lignes)  && (this.colonne_jeton_place - decalage >= 0) && (aligner)) {
                if (! this.grille[this.colonne_jeton_place - decalage].getCase(this.ligne_jeton_place + decalage).existe()) {
                    if (this.grille[this.colonne_jeton_place - decalage].getCase(this.ligne_jeton_place + decalage).getPion().getJoueur() == joueur) {
                    	nombre_aligner++;
                        decalage++;
                    } else {
                        aligner = false;
                    }
                } else {
                    aligner = false;
                }
            }
        }
        return nombre_aligner;
    }
    
    private int verification_diagonales_bas_droite(Joueur joueur) {
         int decalage = 1;
         int nombre_aligner = 0;
         boolean aligner = true;
         if ((this.ligne_jeton_place - decalage >= 0)  && (this.colonne_jeton_place + decalage < this.nombre_colonnes)) {
             while ((this.ligne_jeton_place - decalage >= 0)  && (this.colonne_jeton_place + decalage < this.nombre_colonnes) && (aligner)) {
                 if (! this.grille[this.colonne_jeton_place + decalage].getCase(this.ligne_jeton_place - decalage).existe()) {
                     if (this.grille[this.colonne_jeton_place + decalage].getCase(this.ligne_jeton_place - decalage).getPion().getJoueur() == joueur) {
                    	 nombre_aligner++;
                         decalage++;
                     } else {
                         aligner = false;
                     }
                 } else {
                     aligner = false;
                 }
             }
         }
         return nombre_aligner;
     }
    
    public boolean plein() {
        return this.nombre_cases_vides == 0;
    }
    
}
