package grille;

public class Pion {
	
	private Joueur joueur;
	private char symbole; 	

	public char getSymbole() {
		return symbole;
	}

	public void setSymbole(char symbole) {
		this.symbole = symbole;
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}
	
	public Pion(Joueur joueur) {
		this.joueur = joueur;
		this.symbole = joueur.getSymbole_jeton();
	}

	public Pion(Joueur joueur, char symbole) {
		this.joueur = joueur;
		this.symbole = symbole;
	}

}
