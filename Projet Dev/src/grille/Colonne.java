package grille;

import exception.CaseNonVideException;
import exception.ColonnePleineException;

public class Colonne {
	
	private Case[] colonne;
	private int taille_colonne;
	private int case_bas_vide;

	public Case getCase(int index) {
		if (index < 0 || index >= taille_colonne) {
			throw new IndexOutOfBoundsException();
		}
		return colonne[index];
	}

	public int getTaille_colonne() {
		return taille_colonne;
	}

	public int getCase_bas_vide() {
		return case_bas_vide;
	}
	
	public Colonne(int taille) {
		this.colonne = new Case[taille];
		taille_colonne = taille;
		case_bas_vide = 0;
		for (int index = 0; index < taille; index++) {
			colonne[index] = new Case(); 
		}
	}
	
	public void ajoute_pion(Pion pion) throws IllegalArgumentException, ColonnePleineException {
		if( case_bas_vide >= taille_colonne )
			throw new ColonnePleineException();
		try {
			colonne[case_bas_vide].ajoute_pion(pion);
		} catch (CaseNonVideException e) {
			case_bas_vide++;
			this.ajoute_pion(pion);
		}
		case_bas_vide++;
	}
	
	public boolean plein() {
		return case_bas_vide >= taille_colonne;
	}
	
}