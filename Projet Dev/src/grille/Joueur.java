package grille;

public class Joueur implements Comparable<Joueur> {
	
	private char symbole_jeton;
	private int score_partie;
	private int score_total;
	private String nom;
	private String[] touche_selection;
	
	public char getSymbole_jeton() {
		return symbole_jeton;
	}
	
	public void setSymbole_jeton(char symbole_jeton) {
		this.symbole_jeton = symbole_jeton;
	}
	
	public int getScore_partie() {
		return score_partie;
	}
	
	public void setScore_partie(int score_partie) {
		this.score_partie = score_partie;
	}
	
	public int getScore_total() {
		return score_total;
	}
	
	public void setScore_total(int score_total) {
		this.score_total = score_total;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public Pion getPion() {
		return new Pion(this, symbole_jeton);
	}
	
	public String[] getTouche() {
		return touche_selection;
	}
	
	public void setTouche(int nombreTouche) {
		touche_selection = new String[nombreTouche];
		for (int index = 0; index < nombreTouche; index++) {
			touche_selection[index] = String.valueOf(index + 1);
		}
	}
	
	public void setTouche(String[] touche) {
		this.touche_selection = touche;
	}
	
	public Joueur(String nom, char symbole, int score, String[] touche) {
		this.symbole_jeton = symbole;
		this.score_total = score;
		this.nom = nom;
		this.touche_selection = touche;
	}
	
	public Joueur(char symbole) {
		this.symbole_jeton = symbole;
	}
	
	public Joueur() {
		
	}
	
	public void ajout_score(int score) {
		score_partie += score;
	}
	
	public void reinitialisation_score() {
		sauvegarde_score();
		score_partie = 0;
	}
	
	public void sauvegarde_score() {
		score_total += score_partie;
	}
	
	public int compareTo(Joueur joueur) {
		if (joueur.getScore_partie() >= this.getScore_partie()) {
			if (joueur.getScore_partie() > this.getScore_partie()) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return -1;
		}
	}
}
