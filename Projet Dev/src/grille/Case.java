package grille;

import exception.CaseNonVideException;

public class Case {

	private Pion pion;

	public Pion getPion() {
		return pion;
	}

	public boolean existe() {
		return pion == null ? true : false;
	}
	
	public void ajoute_pion(Pion Pion) throws CaseNonVideException, IllegalArgumentException {
		if (! existe()) {
			throw new CaseNonVideException();
		} if (Pion == null) {
			throw new IllegalArgumentException();
		}
		pion = Pion;
	}
	
	public String toString() {
		StringBuilder constructeur = new StringBuilder();
		constructeur.append("Case [pion=").append(pion).append("]");
		return constructeur.toString();
	}
}
