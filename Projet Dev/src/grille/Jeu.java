package grille;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import exception.ColonnePleineException;
import exception.ToucheExistePasException;

public class Jeu {
	
	private static int nombre_joueur_defaut = 2;
	private static int nombre_partie_defaut = 1;
	
	private Grille grille;
	private int nombre_joueur;
	private int condition_victoire;
	private int nombre_partie;
	private int nombre_partie_restante;
	private int index_joueur_actuel;
	private Joueur[] joueurs;
	private Joueur joueur_actuel;	
	
	public Grille getGrille() {
		return grille;
	}

	public int getNombre_joueur() {
		return nombre_joueur;
	}

	public int getCondition_victoire() {
		return condition_victoire;
	}

	public int getNombre_partie() {
		return nombre_partie;
	}
	
	public int getNombre_partie_restante() {
		return nombre_partie;
	}

	public int getNombre_partie_actuel() {
		return nombre_partie - nombre_partie_restante + 1;
	}
	
	public int getIndex_joueur_actuel() {
		return index_joueur_actuel;
	}

	public Joueur getJoueurs(int index) {
		return joueurs[index];
	}

	public String get_Nom_Joueur_actuel() {
		return joueur_actuel.getNom();
	}
	
	public char get_Symbole_Joueur_actuel() {
		return joueur_actuel.getSymbole_jeton();
	}
	
	public void setGrille(int nombre_colonnes, int nombre_lignes) {
		this.grille = new Grille(nombre_colonnes, nombre_lignes);
	}

	public void setNombre_joueur(int nombre_joueur) {
		this.nombre_joueur = nombre_joueur;
		joueurs = new Joueur[nombre_joueur];
		for (int index = 0; index < nombre_joueur; index++) {
			joueurs[index] = new Joueur();
			joueurs[index].setTouche(grille.getNombre_colonnes());
		}
		setJoueur_actuel(0);
	}

	public void setJoueur_actuel(int index_joueur) {
		index_joueur = index_joueur % nombre_joueur;
		index_joueur_actuel = index_joueur;
		this.joueur_actuel = joueurs[index_joueur];
	}
	
	public void setCondition_victoire(int condition_victoire) {
		this.condition_victoire = condition_victoire;
	}

	public void setNombre_partie(int nombre_partie) {
		this.nombre_partie = nombre_partie;
		this.nombre_partie_restante = nombre_partie;
	}

	public Jeu() {
		grille = new Grille();
		setNombre_joueur(nombre_joueur_defaut);
		setJoueur_actuel(0);
		setNombre_partie(nombre_partie_defaut);
		setCondition_victoire(4);
		joueurs[0].setSymbole_jeton('X');
		joueurs[1].setSymbole_jeton('O');
	}

	public void jouer(String touche, int ordre) throws IllegalArgumentException, ColonnePleineException, ToucheExistePasException {
		String[] touches = joueur_actuel.getTouche();
		int touche_tapper = -1;
		for (int index = 0; index < touches.length; index++) {
			if (touches[index].compareTo(touche) == 0) {
				touche_tapper = index;
			}
		}
		if (touche_tapper == -1) {
			throw new ToucheExistePasException();
		}
		grille.ajouterPion(touche_tapper, joueur_actuel.getPion());
		prochain_tour(ordre);
	}

	public boolean partie_finie(int ordre) {
		boolean test = grille.verification_victoire(getJoueurs(ordre), condition_victoire) || grille.plein();
		return test;
	}
	
	public void prochain_tour(int ordre) {
		if (ordre == 0) {
			setJoueur_actuel(1);
		} else {
			setJoueur_actuel(0);
		}
	}
	
	public void nouvelle_partie() {
		grille.nettoyage_Grille();
		nombre_partie_restante--; 
		joueur_actuel.ajout_score(1);
	}
	
	public String info_resultat() {
		ArrayList<Joueur> classement = new ArrayList<Joueur>();
		for (int index = 0; index < joueurs.length; index++) {
			classement.add(joueurs[index]);
		}
		Collections.sort((List<Joueur>) classement);
		String info_resultat = "";
		info_resultat += "Le joueur " + classement.get(0).getNom() + " est le gagnant !\n";
		info_resultat += "score : " + classement.get(0).getScore_partie() + " / " + this.nombre_partie + "gagnee(s)\n";
		info_resultat += "--- classement ---\n";
		for (int index = 0; index < classement.size(); index++) {
			info_resultat += " - " + index + " : " + classement.get(index).getNom() + " " 
					+  classement.get(index).getScore_partie() + " / " + this.nombre_partie + "gagnee(s)\n";
		}
		return info_resultat;
	}
		
	public void affichage(PrintWriter sortie_socket) {

		for (int ligne = grille.getNombre_lignes() - 1; ligne >= 0 ; ligne--) {
			String affichage = "";
			for (int colonne = 0; colonne < grille.getNombre_colonnes(); colonne++) {
				char symbole = grille.getSymbole(colonne, ligne);
				if (symbole == Character.MIN_VALUE) {
					affichage += "| - ";
				} else {
					affichage += "| " + symbole + " ";
				}
			}
			affichage += "|";
			sortie_socket.println(affichage);
		}
		String[] touches = joueur_actuel.getTouche();
		String affichage_touche = "  ";
		for (String touche : touches) {
			affichage_touche += touche + "   ";
		}
		affichage_touche += "\n";
		sortie_socket.println(affichage_touche);
	}
}
