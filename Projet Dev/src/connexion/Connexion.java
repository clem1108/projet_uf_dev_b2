package connexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Connexion {
	private Connection connexion_base;
	
	public Connexion(String ip, String base, String utilisateur, String mot_passe) {
		try {
			connexion_base = DriverManager.getConnection(
					"jdbc:mysql://" + ip + "/" + base + "?useSSL=false&serverTimezone=UTC", utilisateur, mot_passe);
		} catch(Exception exc) {
			System.err.println(getClass().getSimpleName() + 
					"Constructeur : " + exc.getMessage());
		}
	}
	
	public Connexion(String base) {
		this("localhost", base, "root", "");
	}
	
	public ResultSet requete(String sql) {
		ResultSet resultat = null;
		if( ! sql.contains(" "))
			sql = "SELECT * FROM " + sql;

		Statement declaration;
		try {
			declaration = connexion_base.createStatement();
			resultat = declaration.executeQuery(sql);
		} catch (SQLException exc) {
			System.err.println(getClass().getSimpleName() + 
					"requete("+sql+") : " + exc.getMessage());
		}
		return resultat;
	}

	public int modifier(String sql) {
		int resultat = -1;
		try {
			Statement declaration = connexion_base.createStatement();
			resultat = declaration.executeUpdate(sql);
		} catch(Exception exc) {
			System.err.println(getClass().getSimpleName() + 
					"modifier(" + sql + ") : " + exc.getMessage());
		}
		return resultat;
	}
	public int inserer(String sql) {
		int resultat = -1;
		try {
			Statement declaration = connexion_base.createStatement();
			declaration.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultset = declaration.getGeneratedKeys();
			resultset.next();
			resultat  = resultset.getInt(1);
		} catch(Exception exc) {
			System.err.println(getClass().getSimpleName() + 
					"inserer(" + sql + ") : " + exc.getMessage());
		}
		return resultat ;
	}
}
