package thread;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.ResultSet;

import connexion.Connexion;
import serveur.ServeurJeu;

public class threadJeu implements Runnable {

	private Socket socket;
	private ServeurJeu parent;
	private int ordre;

	public threadJeu(Socket socket, int ordre, ServeurJeu parent) {
		this.socket = socket;
		this.setOrdre(ordre);
		this.parent = parent;
	}
	
	@Override
	public void run() {
		try {
			System.out.println("Un client connect�.");
			Connexion connexion = new Connexion("projet_dev_b2");
			String sql = "SELECT * FROM joueur";
			ResultSet resultat = connexion.requete(sql);
			BufferedReader entrer_socket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter sortie_socket = new PrintWriter(socket.getOutputStream(), true);
			String aide = "Commande possibles : Jouer, Aide ou Salut";
			sortie_socket.println(aide);			
			System.out.println("Message envoy� au client.");
			while(true) {
				String message = entrer_socket.readLine();
				if (message.equals("Salut")) {
					sortie_socket.println("Au revoir");			
					System.out.println("Message envoy� au client.");
					parent.partir();
					break;
				} else if (message.equals("Aide")) {
					sortie_socket.println(aide);
				} else if (message.equals("Jouer")) {
					while(resultat.next()) {
						sortie_socket.println(resultat.getString("nom"));
					}
					if (getOrdre() == 0) {
						parent.partie(entrer_socket, sortie_socket, ordre);
					} else {
						parent.nom_joueur(entrer_socket, sortie_socket, ordre);
					}
					parent.jouer(entrer_socket, sortie_socket, ordre);
				} else {
					sortie_socket.println("Je ne comprends pas");			
					System.out.println("Message envoy� au client.");
				}
			}
			socket.close();
			System.out.println("Socket ferm�.");
			parent.partir();
			
		} catch(Exception exc) {
			System.err.println(exc.getMessage());
		}
		
	}

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}

}
